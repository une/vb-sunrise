' License: GPL-3.0
' (c) 2024 Carlos Une <une@fastmail.fm>
' Reference: https://en.wikipedia.org/wiki/Sunrise_equation
Imports System

Module Program
    Sub Main(args As String())
        Dim loc() = {
            ("AU", "🇦🇺", "Melbourne", -37.82, 144.96),
            ("JP", "🇯🇵", "Tokyo", 35.69, 139.69),
            ("CN", "🇨🇳", "Shanghai", 31.2333, 121.4667),
            ("DE", "🇩🇪", "Berlin", 52.517, 13.383),
            ("GB", "🇬🇧", "London", 51.5, 0.13),
            ("BR", "🇧🇷", "Rio de Janeiro", -22.9, -43.23),
            ("BR", "🇧🇷", "São Paulo", -23.589, -46.658),
            ("US", "🇺🇸", "New York City", 40.7128, -74.0),
            ("US", "🇺🇸", "Seattle", 47.6, -122.33)
        }
        Console.WriteLine("Earth Location;Sunrise (UTC);Sunset (UTC);Day length (seconds);")
        For Each l In loc
            Dim ss As SunriseSunsetResult = SunriseSunset(DateTime.UtcNow, l.Item4, l.Item5)
            Console.WriteLine("{0}{1}/{2};{3};{4};{5};", l.Item2, l.Item3, l.Item1, ToDateStr(ss.Sunrise), ToDateStr(ss.Sunset), ss.DayLength)
        Next
    End Sub

    Structure HourAngle
        Dim CosOmega As Double
        Dim Omega As Double
    End Structure

    Structure SunriseSunsetResult
        Dim Sunrise As Double
        Dim Sunset As Double
        Dim DayLength As Integer
    End Structure

    Function Degrees(a As Double) As Double
        Return a * 180/Math.PI
    End Function

    Function Radians(a As Double) As Double
        Return a * Math.PI/180
    End Function

    Function Sine(a As Double) As Double
        Return Math.Sin(Radians(a))
    End Function

    Function Cosine(a As Double) As Double
        Return Math.Cos(Radians(a))
    End Function

    Function Modf(x As Double, y As Double) As Double
        Return x - Int(x/y) * y
    End Function

    Function MeanSolarTime(n As Double, lw As Double) As Double
        Return n - lw/360.0
    End Function

    Function SolarMeanAnomaly(J As Double) As Double
        Return Modf(357.5291 + 0.98560028 * J, 360.0)
    End Function

    Function EquationOfTheCenter(M As Double) As Double
        Return 1.9148*Sine(M) + 0.02*Sine(2.0*M) + 0.0003*Sine(3.0*M)
    End Function

    Function EclypticLongitude(M As Double, C As Double) As Double
        Return Modf(M + C + 180.0 + 102.9372, 360.0)
    End Function

    Function SolarTransit(J As Double, M As Double, Lambda As Double) As Double
        Return 2451545.0 + J + 0.0053*Sine(M) - 0.0069*Sine(2.0*Lambda)
    End Function

    Function DeclinationOfTheSun(Lambda As Double) As Double
        Return Degrees(Math.Asin(Sine(Lambda)*Sine(23.44)))
    End Function

    Function ComputeHourAngle(phi As Double, delta As Double) As HourAngle
        Dim ha As HourAngle
        ha.CosOmega = (Sine(-0.83) - Sine(phi)*Sine(delta))/(Cosine(phi)*Cosine(delta))
        ha.Omega = Degrees(Math.Acos(ha.CosOmega))
        Return ha
    End Function

    Function JulianDayN(t As DateTime) As Double
        Dim J2000 = new DateTime(2000, 1, 1, 12, 0, 0, 0, DateTimeKind.Utc)
        Return Math.Ceiling(((t - J2000).TotalNanoseconds/1e9/(24.0*3600.0)))
    End Function

    Function SunriseSunset(t As Date, lat As Double, lon As Double) As SunriseSunsetResult
        Dim ss As SunriseSunsetResult
        Dim n As Double = JulianDayN(t)
        Dim J As Double = MeanSolarTime(n, lon)
        Dim M As Double = SolarMeanAnomaly(J)
        Dim C As Double = EquationOfTheCenter(M)
        Dim Lambda As Double = EclypticLongitude(M, C)
        Dim J_transit As Double = SolarTransit(J, M, Lambda)
        Dim decl As Double = DeclinationOfTheSun(Lambda)
        Dim ha As HourAngle = ComputeHourAngle(lat, decl)

        If ha.CosOmega < -1.0 Then
            ss.DayLength = 86400
            return ss
        ElseIf ha.CosOmega > 1.0 Then
            ss.DayLength = 0
            return ss
        End If

        ss.Sunrise = J_transit - ha.Omega/360.0
        ss.Sunset = J_transit + ha.Omega/360.0
        ss.DayLength = Int((ss.Sunset - ss.Sunrise)*86400.0)
        return ss
    End Function

    Function Frac(x As Double) As Double
        Return x - Int(x)
    End Function

    '2415020.5 (Julian Day Number) = 1900-01-01 00:00:00
    Function ToVBDate(j As Double) As DateTime
        Dim days As Double = j - 2415020.5
        Dim d As Date = DateAdd(DateInterval.Day, days, DateValue("January 1, 1900"))
        Return DateAdd(DateInterval.Second, Frac(days) * 86400, d)
    End Function

    Function ToDateStr(j As Double) As String
        Return FormatDateTime(ToVBDate(j), DateFormat.GeneralDate)
    End Function
End Module
