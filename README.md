# 🌞 vb-sunrise 🌞

Compute sunrise and sunset times in VB.NET.

## Run
```
$ dotnet run
Earth Location;Sunrise (UTC);Sunset (UTC);Day length (seconds);
🇦🇺Melbourne/AU;5/21/2024 9:19:19 PM;5/22/2024 7:14:36 AM;35716;
🇯🇵Tokyo/JP;5/21/2024 7:31:36 PM;5/22/2024 9:44:29 AM;51173;
🇨🇳Shanghai/CN;5/21/2024 8:54:34 PM;5/22/2024 10:47:18 AM;49964;
🇩🇪Berlin/DE;5/22/2024 3:00:12 AM;5/22/2024 7:06:24 PM;57971;
🇬🇧London/GB;5/22/2024 3:57:57 AM;5/22/2024 7:54:40 PM;57402;
🇧🇷Rio de Janeiro/BR;5/22/2024 9:22:11 AM;5/22/2024 8:17:21 PM;39309;
🇧🇷São Paulo/BR;5/22/2024 9:37:07 AM;5/22/2024 8:29:51 PM;39163;
🇺🇸New York City/US;5/22/2024 9:32:50 AM;5/23/2024 12:12:52 AM;52801;
🇺🇸Seattle/US;5/22/2024 12:23:35 PM;5/23/2024 3:48:47 AM;55512;
```
